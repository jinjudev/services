package test.example;

import java.util.Scanner;

import com.gta.test.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.gta.bean.QueryDataProto;
import com.gta.bean.QueryDataProto.DataInfo;
import com.gta.bean.QueryDataProto.ExceptionMessage;
import com.gta.bean.QueryDataProto.ReponseQueryData;
import com.gta.bean.QueryDataProto.RequestQueryData;
import com.gta.bean.RequestBean;
import com.gta.bean.SubPubQuoteMessageData;
import com.gta.control.ISDKImpl;
import com.gta.control.SDKImpl;
import com.gta.util.Constant;
import com.gta.util.DateUtil;

public class SDKImplTest {

	private static Logger logger = LoggerFactory.getLogger(SDKImplTest.class);

	static ISDKImpl sdk = null;

	public static void main(String[] args) {

		sdk = SDKImpl.getInstance();
		int error_Code = sdk.start();
		System.out.println("start_code:" + error_Code);

		int conn_status = sdk.getConnectStatus();
		System.out.println("conn_status:" + conn_status);

		Scanner scanner = null;

		int timeout = Constant.TIMEOUT;

        testQuerySyc(formatArgs("000001.SZSE", "093000", "100000"), timeout);
        scanner = new Scanner(System.in);
        String str = scanner.next();
        while (!str.equalsIgnoreCase("exit") && !str.equals("e")) {
            System.out.println("输入 exit(e)退出");
            str = scanner.next();
        }
        sdk.stop();

	/*	if (args == null || args.length < 2) {
			info();
		} else if ((args[0].equals("QuerySyc") || args[0].equals("qs"))
				&& args.length == 4) {

			testQuerySyc(formatArgs(args[1], args[2], args[3]), timeout);

			System.out.println("输入 exit(e)退出");
			scanner = new Scanner(System.in);
			String str = scanner.next();
			while (!str.equalsIgnoreCase("exit") && !str.equals("e")) {
				System.out.println("输入 exit(e)退出");
				str = scanner.next();
			}
			sdk.stop();
		} else if ((args[0].equals("Query") || args[0].equals("q"))
				&& args.length == 4) {
			testQuery(formatArgs(args[1], args[2], args[3]), timeout);
		} else if (args[0].equals("Subscribe") || args[0].equals("s")) {

			testSubscribe(formatArgs(args[1]), timeout, false);

			System.out.println("输入 exit(e)退出");
			scanner = new Scanner(System.in);
			String str = scanner.next();
			while (!str.equalsIgnoreCase("exit") && !str.equals("e")) {
				System.out.println("输入 exit(e)退出");
				str = scanner.next();
			}
			sdk.stop();
		} else if (args[0].equals("SubscribeSave") || args[0].equals("ss")) {
			testSubscribe(formatArgs(args[1]), timeout, true);

			System.out.println("输入 exit(e)退出");
			scanner = new Scanner(System.in);
			String str = scanner.next();
			while (!str.equalsIgnoreCase("exit") && !str.equals("e")) {
				System.out.println("输入 exit(e)退出");
				str = scanner.next();
			}
			sdk.stop();
		} else {
			info();
		}*/

	}

	public static void info() {
		System.out
				.println("**********************************************************************");
		System.out.println("example:");
		System.out
				.println("Query: java -jar xxx.jar Query(q) SymbolArray startTime endTime");
		System.out
				.println("Query: java -jar xxx.jar Query(q) '000001.SZSE' 093000 100000");
		System.out
				.println("Subscribe(print): java -jar xxx.jar Subscribe(s) SymbolArray");
		System.out
				.println("Subscribe(print): java -jar xxx.jar Subscribe(s) '*.SSE,*.SZSE'");
		System.out
				.println("Subscribe(save): java -jar xxx.jar SubscribeSave(ss) SymbolArray");
		System.out
				.println("Subscribe(save): java -jar xxx.jar SubscribeSave(ss) '*.SSE,*.SZSE'");
		System.out
				.println("**********************************************************************");
	}

	public static RequestBean formatArgs(String symbolArray) {
		RequestBean requestBean = new RequestBean();

		String[] sa = symbolArray.split(",");
		requestBean.setSymbolArray(sa);

		return requestBean;
	}

	public static RequestBean formatArgs(String symbolArray, String starTime,
			String endTime) {
		RequestBean requestBean = new RequestBean();
		String date = DateUtil.format(DateUtil.dateNow(),
				DateUtil.FORMAT_YEAR_MONTH_DAY);

		starTime = date + starTime;
		requestBean.setStartTime(starTime);

		endTime = date + endTime;
		requestBean.setEndTime(endTime);

		String[] sa = symbolArray.split(",");
		requestBean.setSymbolArray(sa);

		return requestBean;
	}

	/**
	 * 异步查询
	 * */
	public static void testQuerySyc(RequestBean requestBean, int timeout) {

		// 1. 初始化
		// ISDKImpl sdk = SDKImpl.getInstance();
		// int error_Code = sdk.start();
		// System.out.println("start_code:" + error_Code);
		//
		// int conn_status = sdk.getConnectStatus();
		// System.out.println("conn_status:" + conn_status);

		// sdk.regConnectStatusEvent(new ConnStatusConsumer());

		// // 2. 构造查询条件
		// String startTime = "20160304080000";
		// String endTime = "201603041500000";
		//
		// String[] symbolArray = new String[] { "600885.SSE" };
		//
		// RequestBean requestBean = new RequestBean();
		// requestBean.setSymbolArray(symbolArray);
		// requestBean.setStartTime(startTime);
		// requestBean.setEndTime(endTime);

		try {
			// getRequestByte只用于查询构造查询条件byte[]
			// 初始化失败，或查询条件设置错误，构建结果为NULL。

			RequestQueryData.Builder builder = sdk
					.getRequestQueryBuilder(requestBean);
			if (builder == null) {
				System.out.println("查询条件构建失败");
				return;
			}
			int requestID = sdk
					.request(builder, timeout, new test.example.RequestConsumer());
			System.out.println("requestID=" + requestID);

		} catch (Exception e) {
			e.printStackTrace();
		}

		// sdk.stop();
	}

	/**
	 * 同步查询
	 * */
	public static void testQuery(RequestBean requestBean, int timeout) {

		// 1. 初始化
		// ISDKImpl sdk = SDKImpl.getInstance();
		// int error_Code = sdk.start();
		// System.out.println("start_code:" + error_Code);
		//
		// int conn_status = sdk.getConnectStatus();
		// System.out.println("conn_status:" + conn_status);

		// // 2. 构造查询条件
		// String startTime = "20160302093000";
		// String endTime = "201603020931000";
		// String[] symbolArray = new String[] { "600885.SSE" };
		//
		// RequestBean requestBean = new RequestBean();
		// requestBean.setSymbolArray(symbolArray);
		// requestBean.setStartTime(startTime);
		// requestBean.setEndTime(endTime);

		int i = 0;
		try {
			// getRequestByte只用于查询构造查询条件byte[]
			RequestQueryData.Builder builder = sdk
					.getRequestQueryBuilder(requestBean);
			if (builder == null) {
				System.out.println("查询条件构建失败");
				return;
			}
			Object obj = sdk.request(builder, timeout);

			if (obj != null) {

				if (obj.getClass() == ReponseQueryData.class) {

					ReponseQueryData data = (ReponseQueryData) obj;

					ExtensionRegistry registry = ExtensionRegistry
							.newInstance();

					SubPubQuoteMessageData.registerAllExtensions(registry);

					SubPubQuoteMessageData.SubPubQuoteMessage sub = null;

					try {
						if (data.getListDataInfoList().size() == 0) {
							System.out.println("查询结果为空");
						} else {
							for (DataInfo ldata : data.getListDataInfoList()) {
								sub = SubPubQuoteMessageData.SubPubQuoteMessage
										.parseFrom(ldata.getData(), registry);
								System.out.println(sub);
								i++;
								logger.info("接收到第" + i + "条数据");
							}
						}

					} catch (InvalidProtocolBufferException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (obj.getClass() == ExceptionMessage.class) {
					ExceptionMessage exceptionMessage = (ExceptionMessage) obj;
					System.out.println("error_code:"
							+ exceptionMessage.getExceptionCode());
				} else if (obj.getClass() == Integer.class) {
					System.out.println("error_code:" + (Integer) obj);
				}
			} else {
				System.out.println("obj is null");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		sdk.stop();

	}

	/**
	 * 订阅测试
	 */
	public static void testSubscribe(RequestBean requestBean, int timeout,
			boolean isSave) {

		// 1.初始化
		// ISDKImpl sdk = SDKImpl.getInstance();
		// int error_Code = sdk.start();
		// System.out.println("start_code:" + error_Code);
		//
		// int conn_status = sdk.getConnectStatus();
		// System.out.println("conn_status:" + conn_status);

		sdk.regSubscribeEvent(new SubscribeConsumer(isSave));
		// 订阅
		// error_Code = sdk.subscribe(new String[] { "600885.SSE" },
		// ISDKImpl.CMD_TYPE_SUB_PUB_MERGE, 100);
		// error_Code = sdk.subscribe(new String[] { "*.SSE", "*.SZSE" },
		// ISDKImpl.CMD_TYPE_SUB_PUB_MERGE, 50);
		int error_Code = sdk.subscribe(requestBean.getSymbolArray(),
				ISDKImpl.CMD_TYPE_SUB_PUB_MERGE, timeout);
		System.out.println("subscribe result:" + error_Code);
		// 取消订阅
		// sdk.subscribe(new String[] { "600885.SSE" },
		// ISDKImpl.CMD_TYPE_SUB_PUB_MERGE_UNSUB, 50);

		// todo注册回调事件

		// 5. 当不再使用本接口，程序退出时，执行 stop()方法，以停止服务；
		// sdk.stop();
		// logger.info("与dsp之间的测试---->结束");
	}

}
