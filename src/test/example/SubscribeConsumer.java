package test.example;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import com.gta.bean.QueryDataProto;
import com.gta.bean.QueryDataProto.ExceptionMessage;
import com.gta.bean.SubPubQuoteMessageData;
import com.gta.callback.ISubscribeCallback;

public class SubscribeConsumer implements ISubscribeCallback {

	Queue<SubPubQuoteMessageData.SubPubQuoteMessage> queue = new LinkedList<SubPubQuoteMessageData.SubPubQuoteMessage>();
	private boolean isSave;

	public SubscribeConsumer(boolean isSave) {
		this.isSave = isSave;
		if (isSave) {
			Thread thread = new SaveDataThread();
			thread.setDaemon(true);
			thread.start();
		}
	}

	public void deal(Object obj) {
		// TODO Auto-generated method stub

		if (obj != null) {
			if (obj.getClass() == SubPubQuoteMessageData.SubPubQuoteMessage.class) {

				SubPubQuoteMessageData.SubPubQuoteMessage subPubQuoteMessage = (SubPubQuoteMessageData.SubPubQuoteMessage) obj;

				if (isSave)
					synchronized (queue) {
						queue.offer(subPubQuoteMessage);
					}
				else {
					System.out.println(subPubQuoteMessage);
				}

			} else if (obj.getClass() == ExceptionMessage.class) {
				ExceptionMessage exceptionMessage = (ExceptionMessage) obj;
				System.out.println("error_code:"
						+ exceptionMessage.getExceptionCode());
			}
		}
	}

	private class SaveDataThread extends Thread {

		public void createFile(long seqID) {

			File file = new File("SubFile/");
			// 如果文件夹不存在则创建
			if (!file.exists() && !file.isDirectory()) {
				file.mkdir();
			}

			// 创建一个文件对象
			File datafile = new File("SubFile/SubscribeFile" + seqID + ".txt");

			if (datafile.exists() && datafile.isFile()) {
				System.out.println("使用已经存在的SubscribeFile" + seqID + ".txt文件");
			} else {
				try {
					// 创建文件
					datafile.createNewFile();
					System.out.println("创建SubscribeFile" + seqID + ".txt文件");
				} catch (IOException e) {
					System.out.println("创建SubscribeFile" + seqID
							+ ".txt文件失败,错误信息：" + e.getMessage());
				}
			}

		}

		long num = 0;
		long local_seqID = 0;
		long timespan = 5 * 60 * 1000; // 五分钟
		long timeout = 0;
		long cal_timeout = 0;
		long unix_timeout = 0;
		long out_timeout = 0;
		FileWriter pw;

		public String getFileName() {
			long seqID = System.currentTimeMillis() / timespan;
			if (seqID != local_seqID) {
				createFile(seqID);
				local_seqID = seqID;
			}
			return "SubFile/SubscribeFile" + seqID + ".txt";
		}

		public void run() {
			// TODO Auto-generated method stub

			while (true) {

				synchronized (queue) {
					try {
						if (queue != null && queue.size() != 0) {
							pw = new FileWriter(getFileName(), true);
							SubPubQuoteMessageData.SubPubQuoteMessage sub;
							while ((sub = queue.poll()) != null) {
								// long curtime = System.currentTimeMillis();
								// Long in = sub
								// .getExtension(SubPubQuoteMessageData.timeStampProducerIn);
								Long out = sub
										.getExtension(SubPubQuoteMessageData.timeStampProducerOut);
								// Long unix = sub
								// .getExtension(SubPubQuoteMessageData.uNIX);
								// cal_timeout = cal_timeout + (out - in);
								// timeout = timeout + (curtime - in);
								// unix_timeout = unix_timeout + (curtime -
								// unix);
								// out_timeout = out_timeout + (curtime - out);
								num++;
								pw.write(sub.toString() + num + "条");
							}
							// pw.write("计算平均时间延迟：" + (cal_timeout / num) +
							// "\n");
							// pw.write("开始计算到客户平均时间延迟：" + (timeout / num) +
							// "\n");
							// pw.write("结束计算到客户平均时间延迟：" + (out_timeout / num)
							// + "\n");
							// pw.write("交易产生到客户平均时间延迟：" + (unix_timeout / num)
							// + "\n");
							pw.close();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				try {
					Thread.sleep(3000); // 3秒刷一下缓存到文件
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

}
