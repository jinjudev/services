package test.example;

import com.gta.test.SDKImplTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.gta.bean.QueryDataProto;
import com.gta.bean.QueryDataProto.DataInfo;
import com.gta.bean.QueryDataProto.ExceptionMessage;
import com.gta.bean.QueryDataProto.ReponseQueryData;
import com.gta.bean.SubPubQuoteMessageData;
import com.gta.callback.IRequestCallback;

public class RequestConsumer implements IRequestCallback {

	private static Logger logger = LoggerFactory.getLogger(SDKImplTest.class);

	int i = 0;

	public void deal(Object obj) {
		// To change body of implemented methods use File |
		// Settings | File Templates.

		if (obj != null) {

			if (obj.getClass() == ReponseQueryData.class) {

				ReponseQueryData data = (ReponseQueryData) obj;

				ExtensionRegistry registry = ExtensionRegistry.newInstance();

				SubPubQuoteMessageData.registerAllExtensions(registry);

				SubPubQuoteMessageData.SubPubQuoteMessage sub = null;

				try {
					if (data.getListDataInfoList().size() == 0) {
						System.out.println("查询结果为空");
					} else {
						for (DataInfo ldata : data.getListDataInfoList()) {

							sub = SubPubQuoteMessageData.SubPubQuoteMessage
									.parseFrom(ldata.getData(), registry);
							System.out.println(sub);
							i++;
							logger.info("接收到第" + i + "条数据");

						}
					}
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (obj.getClass() == ExceptionMessage.class) {
				ExceptionMessage exceptionMessage = (ExceptionMessage) obj;
				System.out.println("error_code:"
						+ exceptionMessage.getExceptionCode());
			}
		}
	}

}
