package com.jinju.gta;

import com.google.protobuf.Descriptors;
import com.gta.bean.SubPubQuoteMessageData;
import org.bson.Document;

import java.util.Iterator;
import java.util.Map;

public class Util {
    public static Document parseToDocument(SubPubQuoteMessageData.SubPubQuoteMessage sub) {
        Document doc = new Document();
        Map map = sub.getAllFields();
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            Descriptors.FieldDescriptor k = (Descriptors.FieldDescriptor) it.next();
            doc.append(k.getName(), map.get(k));
        }
        return doc;
    }
}
