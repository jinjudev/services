package com.jinju.gta;

import com.gta.bean.QueryDataProto.ExceptionMessage;
import com.gta.bean.SubPubQuoteMessageData;
import com.gta.callback.ISubscribeCallback;
import com.gta.util.DateUtil;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.mongodb.morphia.Morphia;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

import static com.mongodb.client.model.Filters.eq;

public class StockSubscribeConsumer implements ISubscribeCallback {

    Queue<SubPubQuoteMessageData.SubPubQuoteMessage> queue = new LinkedList<SubPubQuoteMessageData.SubPubQuoteMessage>();

    public StockSubscribeConsumer() {

        Thread thread = new SaveDataThread();
        thread.setDaemon(true);
        thread.start();

    }

    public void deal(Object obj) {
        // TODO Auto-generated method stub

        if (obj != null) {
            if (obj.getClass() == SubPubQuoteMessageData.SubPubQuoteMessage.class) {

                SubPubQuoteMessageData.SubPubQuoteMessage subPubQuoteMessage = (SubPubQuoteMessageData.SubPubQuoteMessage) obj;

                synchronized (queue) {
                    queue.offer(subPubQuoteMessage);
                }

                // System.out.println(subPubQuoteMessage);

            } else if (obj.getClass() == ExceptionMessage.class) {
                ExceptionMessage exceptionMessage = (ExceptionMessage) obj;
                System.out.println("error_code:"
                        + exceptionMessage.getExceptionCode());
            }
        }
    }

    private class SaveDataThread extends Thread {



        long num = 0;
        long local_seqID = 0;
        long timespan = 5 * 60 * 1000; // 五分钟
//        FileWriter pw;
        ArrayList list;

        public void run() {
            // TODO Auto-generated method stub

            while (true) {

                synchronized (queue) {
                    try {
                        if (queue != null && queue.size() != 0) {
//                            pw = new FileWriter(getFileName(), true);

                            list = new ArrayList();
                            SubPubQuoteMessageData.SubPubQuoteMessage sub;
                            while ((sub = queue.poll()) != null) {
//                                num++;
//                                pw.write(sub.toString() + "   第" + num + "条数据");

//                                list.add(Util.parseToDocument(sub));


                                Document d = Util.parseToDocument(sub);

//                                System.out.println(d.toJson());
//                                System.out.println(sub);
//                                System.out.println("====================");
                                StockService.updateOne(d);
//                                System.out.println(sub);
                            }
//                            pw.close();

//                            StockService.save(list);
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                try {
                    Thread.sleep(3000); // 3秒刷一下缓存到文件
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }

    }

}
