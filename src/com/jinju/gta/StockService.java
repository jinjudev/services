package com.jinju.gta;

import com.gta.util.DateUtil;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.converters.StringConverter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.descending;

public class StockService {
    private static MongoClient mongoClient = MongoClientFactory.getInstance();
    private static MongoDatabase database = mongoClient.getDatabase("test");
    private static MongoCollection<Document> collection = database.getCollection("stocks");
    private static MongoCollection<Document> stats = database.getCollection("stat");
    private static MongoCollection<Document> query = database.getCollection("query");
    private static MongoCollection<Document> realtime = database.getCollection("realtime");
    private static MongoCollection<Document> stockinfo = database.getCollection("stockinfo");

    public static void save(Document doc) {
        collection.insertOne(doc);
    }

    public static void save(List<Document> docs) {
        collection.insertMany(docs);
    }

    public static void updateOne(Document updateDoc) throws ParseException {
        Document newDoc = new Document();
        String timeStr = updateDoc.getString("TradingTime").substring(0, 16);
        Date time = DateUtil.format(timeStr, "yyyy-MM-dd HH:mm");
        String _symbol = updateDoc.getString("Symbol");
        String _market = updateDoc.getString("Market");
        String symbol=_market.equals("SSE")?"sh"+_symbol:"sz"+_symbol;
        newDoc.append("symbol",symbol);
        newDoc.append("time", time.getTime());
        newDoc.append("price",updateDoc.getDouble("Lastprice"));
        newDoc.append("volume",updateDoc.getLong("TradeVolume"));
        newDoc.append("open", updateDoc.getDouble("Openprice"));
        newDoc.append("close",updateDoc.getDouble("PreClosePrice"));
        newDoc.append("high", updateDoc.getDouble("Highprice"));
        newDoc.append("low", updateDoc.getDouble("Lowprice"));
        ArrayList filters = new ArrayList();
        filters.add(eq("symbol", symbol));
        filters.add(eq("time", time.getTime()));
        updateDoc.append("symbol",symbol);

//        System.out.println(newDoc.toJson());
        realtime.updateOne(Filters.and(filters), new Document("$set",newDoc), new UpdateOptions().upsert(true));
        stockinfo.updateOne(eq("symbol", symbol), new Document("$set",updateDoc), new UpdateOptions().upsert(true));

    }

    public static void insertRealtimeData() {
        ArrayList list=SDKService.queryData(SDKService.formatArgs("600570.SSE","093000", "160000"));
        System.out.println(list.size());
        Iterator<Document> cursor = list.iterator();
        try {
            while (cursor.hasNext()) {
                Document updateDoc = cursor.next();
                Document newDoc = new Document();
                String timeStr = updateDoc.getString("TradingTime").substring(0, 16);
                Date time = DateUtil.format(timeStr, "yyyy-MM-dd HH:mm");
                String _symbol = updateDoc.getString("Symbol");
                String _market = updateDoc.getString("Market");
                String symbol=_market.equals("SSE")?"sh"+_symbol:"sz"+_symbol;
                newDoc.append("symbol",symbol);
                newDoc.append("time", time.getTime());
                newDoc.append("price",updateDoc.getDouble("Lastprice"));
                newDoc.append("volume",updateDoc.getLong("TradeVolume"));
                newDoc.append("open", updateDoc.getDouble("Openprice"));
                newDoc.append("close",updateDoc.getDouble("PreClosePrice"));
                newDoc.append("high", updateDoc.getDouble("Highprice"));
                newDoc.append("low", updateDoc.getDouble("Lowprice"));
                ArrayList filters = new ArrayList();
                filters.add(eq("symbol", symbol));
                filters.add(eq("time", time.getTime()));
                updateDoc.append("symbol",symbol);




//                Document d = cursor.next();
//                String timeStr = d.getString("TradingTime").substring(0, 16);
//                Date time = DateUtil.format(timeStr, "yyyy-MM-dd HH:mm");
//                d.append("TradingTime", time.getTime());
//                d.remove("_id");
//                d.remove("Freq");
//                d.remove("SecurityID");
//                ArrayList filters = new ArrayList();
//                filters.add(eq("Symbol", d.getString("Symbol")));
//                filters.add(eq("Market", d.getString("Market")));
//                filters.add(eq("TradingTime", time.getTime()));
                realtime.updateOne(Filters.and(filters), new Document("$set", newDoc), new UpdateOptions().upsert(true));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            cursor.close();
        }

    }

    public static void queryStatAll() {
        long count = stats.count();
        MongoCursor<Document> cursor = stats.find().sort(descending("count")).iterator();


//        ArrayList<String> _symbols = new ArrayList<String>();

        try {
            while (cursor.hasNext()) {
                Document d = cursor.next();
//                _symbols.add(d.getString("symbol")+"."+d.getString("market"));

                Document _new = new Document();
                String symbol = d.getString("symbol") + "." + d.getString("market");
                System.out.println(symbol);
                ArrayList<Document> list = SDKService.queryData(SDKService.formatArgs(symbol, "093000", "160000"));
                int size = list.size();
                if (size > 0) {
                    Document first = list.get(0);
                    Document last = list.get(size - 1);
                    _new.append("firstTime", DateUtil.format(first.getString("TradingTime")));
                    _new.append("lastTime", DateUtil.format(last.getString("TradingTime")).getMinutes());
                    _new.append("symbol", first.getString("Symbol"));
                    _new.append("name", first.getString("ShortName"));
                    _new.append("count", size);
//                query.insertOne(_new);
                    System.out.println(_new.toJson());
                }
//                String[] arg=new String[]{};

//                ArrayList<Document> result= SDKService.queryData(SDKService.formatArgs(_symbols.toArray(new String[]{}), "093000", "160000"));

            }
//            String[] arg=new String[]{"600570.SSE","600000.SSE"};
//            ArrayList<Document> result= SDKService.queryData(SDKService.formatArgs(_symbols.toArray(new String[]{}), "093000", "160000"));
//            ArrayList<Document> result= SDKService.queryData(SDKService.formatArgs(arg, "093000", "160000"));
//            System.out.println(result.size());
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            cursor.close();
            SDKService.stop();
        }
    }
}
