package com.jinju.gta;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.util.Date;
import java.util.List;

@Entity("stocks")
@Indexes(
        @Index(value = "salary", fields = @Field("salary"))
)
public class Stock {
    @Id
    private ObjectId id;
    private String name;
    private String symbol;
    private Date tradingTime;
    private Date tradingDate;
//    @Property("wage")
    private Double preClosePrice;
    private Double openPrice;
    private Double highPrice;
    private Double lowPrice;
    private Double lastPrice;
    private Double totalVolume;
    private Double tradeVolume;
    private Long totalAmount;
    private Long tradeAmount;
    private String buyOrSell;
    private Double SellPrice01;
    private Double SellPrice02;
    private Double SellPrice03;
    private Double SellPrice04;
    private Double SellPrice05;


    public Stock() {
    }

    public Stock(String s, double v) {
        this.name = s;
    }
}