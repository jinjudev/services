package com.jinju.gta;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;

public class MongoClientFactory {
    private static MongoClient instance = null;

    static {
        instance = new MongoClient("localhost", getOptions());
    }

    public static MongoClient getInstance() {
        return instance;
    }

    private static MongoClientOptions getOptions() {
        MongoClientOptions options = null;
        MongoClientOptions.Builder build = new MongoClientOptions.Builder();
        build.connectionsPerHost(50);   //与目标数据库能够建立的最大connection数量为50
        build.threadsAllowedToBlockForConnectionMultiplier(50); //如果当前所有的connection都在使用中，则每个connection上可以有50个线程排队等待
        build.maxWaitTime(1000 * 60 * 2);
        build.connectTimeout(1000 * 60 * 1);    //与数据库建立连接的timeout设置为1分钟
        build.socketTimeout(1000 * 60 * 5);
        options = build.build();
        return options;
    }
}
