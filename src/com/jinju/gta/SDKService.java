package com.jinju.gta;

import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.gta.bean.QueryDataProto;
import com.gta.bean.RequestBean;
import com.gta.bean.SubPubQuoteMessageData;
import com.gta.control.ISDKImpl;
import com.gta.control.SDKImpl;

import com.gta.util.DateUtil;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.example.SubscribeConsumer;

import java.util.ArrayList;

public class SDKService {
    private static Logger logger = LoggerFactory.getLogger(SDKService.class);
    private static ISDKImpl sdk;
    public static int connectStatus = -1;

    static {
        start();
    }

    private static ISDKImpl getSDK() {
        if (sdk == null) {
            sdk = SDKImpl.getInstance();
            return sdk;
        }
        return sdk;
    }

    private static void start() {
        int error_Code = getSDK().start();
        System.out.println("start_code:" + error_Code);

        connectStatus = getSDK().getConnectStatus();
        System.out.println("conn_status:" + connectStatus);
    }
    public static void stop() {
        sdk.stop();
        System.out.println("stop" );

    }

    public static ArrayList<Document> queryData( RequestBean requestBean) {
//        RequestBean requestBean = formatArgs(symbolArray, starTime, endTime);
        ArrayList list = new ArrayList();
        int i = 0;
        try {
            // getRequestByte只用于查询构造查询条件byte[]
            QueryDataProto.RequestQueryData.Builder builder = sdk
                    .getRequestQueryBuilder(requestBean);
            Object obj = sdk.request(builder, 50);

            if (obj != null) {

                if (obj.getClass() == QueryDataProto.ReponseQueryData.class) {

                    QueryDataProto.ReponseQueryData data = (QueryDataProto.ReponseQueryData) obj;

                    ExtensionRegistry registry = ExtensionRegistry
                            .newInstance();

                    SubPubQuoteMessageData.registerAllExtensions(registry);

                    SubPubQuoteMessageData.SubPubQuoteMessage sub = null;

                    try {
                        if (data.getListDataInfoList().size() == 0) {
                            System.out.println("查询结果为空");
                        } else {
                            for (QueryDataProto.DataInfo ldata : data.getListDataInfoList()) {
                                sub = SubPubQuoteMessageData.SubPubQuoteMessage
                                        .parseFrom(ldata.getData(), registry);
//                                System.out.println(sub);
                                list.add(Util.parseToDocument(sub));
                                i++;
                                logger.info("接收到第" + i + "条数据");
                            }
                        }

                    } catch (InvalidProtocolBufferException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else if (obj.getClass() == QueryDataProto.ExceptionMessage.class) {
                    QueryDataProto.ExceptionMessage exceptionMessage = (QueryDataProto.ExceptionMessage) obj;
                    System.out.println("error_code:"
                            + exceptionMessage.getExceptionCode());
                } else if (obj.getClass() == Integer.class) {
                    System.out.println("error_code" + (Integer) obj);
                }
            }

        } catch (Exception e) {
            sdk.stop();
            e.printStackTrace();
        }

//        sdk.stop();
        return list;

    }

    public static void subscribeData(String symbolArray) {
        RequestBean requestBean = formatArgs(symbolArray);
        sdk.regSubscribeEvent(new StockSubscribeConsumer());
        // 订阅
        // error_Code = sdk.subscribe(new String[] { "600885.SSE" },
        // ISDKImpl.CMD_TYPE_SUB_PUB_MERGE, 100);
        // error_Code = sdk.subscribe(new String[] { "*.SSE", "*.SZSE" },
        // ISDKImpl.CMD_TYPE_SUB_PUB_MERGE, 50);
        int error_Code = sdk.subscribe(requestBean.getSymbolArray(),
                ISDKImpl.CMD_TYPE_SUB_PUB_MERGE, 50);
        System.out.println("subscribe result:" + error_Code);
        // 取消订阅
        // sdk.subscribe(new String[] { "600885.SSE" },
        // ISDKImpl.CMD_TYPE_SUB_PUB_MERGE_UNSUB, 50);

        // 休眠，等待回调执行
        while (true) {
            try {
                // logger.info("开始：" + sdf.format(new Date()));
                Thread.sleep(100 * 1000);

                // logger.info("结束：" + sdf.format(new Date()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // todo注册回调事件

        // 5. 当不再使用本接口，程序退出时，执行 stop()方法，以停止服务；
        // sdk.stop();
        // logger.info("与dsp之间的测试---->结束");
    }

    public static RequestBean formatArgs(String symbolArray, String starTime,
                                         String endTime) {
        RequestBean requestBean = new RequestBean();
        String date = DateUtil.format(DateUtil.dateNow(),
                DateUtil.FORMAT_YEAR_MONTH_DAY);

        starTime = date + starTime;
        requestBean.setStartTime(starTime);

        endTime = date + endTime;
        requestBean.setEndTime(endTime);

        String[] sa = symbolArray.split(",");
        requestBean.setSymbolArray(sa);

        return requestBean;
    }
    public static RequestBean formatArgs(String[] symbolArray, String starTime,
                                         String endTime) {
        RequestBean requestBean = new RequestBean();
        String date = DateUtil.format(DateUtil.dateNow(),
                DateUtil.FORMAT_YEAR_MONTH_DAY);

        starTime = date + starTime;
        requestBean.setStartTime(starTime);

        endTime = date + endTime;
        requestBean.setEndTime(endTime);

        String[] sa = symbolArray;
        requestBean.setSymbolArray(sa);

        return requestBean;
    }

    public static RequestBean formatArgs(String symbolArray) {
        RequestBean requestBean = new RequestBean();

        String[] sa = symbolArray.split(",");
        requestBean.setSymbolArray(sa);

        return requestBean;
    }
}
